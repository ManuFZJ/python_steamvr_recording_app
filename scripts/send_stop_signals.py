#!/usr/bin/env python
# coding: utf-8

import sys
import win32evtlogutil
import win32evtlog
import time

def accurate_delay(delay):
    _ = time.perf_counter() + delay/1000
    while time.perf_counter() < _:
        pass

# prep some stuff
delay = 1000
id = 7040  # can be any number as far as I can tell

# TrialEnd
event_name = "ResponseEvent" # name of event to stop trial
win32evtlogutil.ReportEvent(event_name, id, eventType=win32evtlog.EVENTLOG_WARNING_TYPE)

accurate_delay(delay) # to ensure proper trial finish

# End
event_name = "End" # name of event to stop recording app
win32evtlogutil.ReportEvent(event_name, id, eventType=win32evtlog.EVENTLOG_WARNING_TYPE)