#!/usr/bin/env python
# coding: utf-8

import os
import sys
from pathlib import Path
import time
from datetime import datetime
import math
import win32evtlog
import numpy as np
import pandas as pd
import openvr
vr_system = openvr.init(openvr.VRApplication_Background)

def get_steamvr_raw_vals(record_pos):
    vr_sys = openvr.IVRSystem()
    poses = vr_sys.getDeviceToAbsoluteTrackingPose(openvr.TrackingUniverseStanding, 0.0, [openvr.TrackedDevicePose_t])
    
    hmd_pose = poses[openvr.k_unTrackedDeviceIndex_Hmd]
    matrix = hmd_pose.mDeviceToAbsoluteTracking
    
    pose = {}
    position = {}
    position['x'] = matrix[0][3]
    position['y'] = matrix[1][3]
    position['z'] = matrix[2][3]
    q = {}
    q['w'] = math.sqrt(max(0, 1 + matrix[0][0] + matrix[1][1] + matrix[2][2])) / 2.0
    q['x'] = math.sqrt(max(0, 1 + matrix[0][0] - matrix[1][1] - matrix[2][2])) / 2.0
    q['y'] = math.sqrt(max(0, 1 - matrix[0][0] + matrix[1][1] - matrix[2][2])) / 2.0
    q['z'] = math.sqrt(max(0, 1 - matrix[0][0] - matrix[1][1] + matrix[2][2])) / 2.0
    q['x'] = math.copysign(q['x'], matrix[2][1] - matrix[1][2])
    q['y'] = math.copysign(q['y'], matrix[0][2] - matrix[2][0])
    q['z'] = math.copysign(q['z'], matrix[1][0] - matrix[0][1])
    
    # x: left (-), right (+), y: up (+) down (-), z: front (-) back (+)
    # all in relation to calibration position
    pose['position'] = position
    
    # x: pitch, y: yaw, z: roll
    # with w conversion to quaternion / rotators possible
    pose['orientation'] = q
    
    if record_pos:
        return [position['x'], position['y'], position['z']]
    else:
        return [q['x'], q['y'], q['z'], q['w']]

def check_for_event(event_name, after_time):
    handle = h=win32evtlog.OpenEventLog(os.environ['COMPUTERNAME'], "Application")
    flags = win32evtlog.EVENTLOG_BACKWARDS_READ|win32evtlog.EVENTLOG_SEQUENTIAL_READ
    events = win32evtlog.ReadEventLog(handle, flags,0)

    event_found = False
    
    for event in events:
        if event.TimeGenerated > after_time and event.SourceName == event_name:
            event_found = True
            break

    return event_found

def exp_loop(filename, record_pos):
    # prep exp start
    exp_started = True
    exp_start_time = datetime.now() # have to use daytime, since events use it

    # prep data collection
    x = []
    y = []
    z = []
    w = []
    times = [] # will be in nanoseconds
    trial_nums = []
    hz_arr = []

    # only consider trial starts after this moment
    trials_start_time = exp_start_time
    trial_num = 0
    print("Exp Loop started")

    while exp_started:
        trial_started = False

        if check_for_event("StimulusOnset", trials_start_time):
            print("Trial started")
            trial_started = True
            trials_start_time = datetime.now() # next trial start has to start this one
            trial_num = trial_num + 1
            start_time = time.time_ns()

        # collect data for trial duration
        while trial_started:
            rec_time = time.time_ns()
            new_time = (rec_time - start_time)
            
            rot = get_steamvr_raw_vals(record_pos)
            x.append(rot[0])
            y.append(rot[1])
            z.append(rot[2])
            w.append(rot[len(rot) - 1]) # will be z if position recorded
            times.append(new_time)
            trial_nums.append(trial_num)

            # check if data collecting needs to pause
            if check_for_event("ResponseEvent", trials_start_time):
                trial_started = False
                
                # check how much Hz we reached in this trial
                trial_dur = (rec_time - start_time) / 1_000_000 # from ns to ms
                hz_last_trial = round(1000 / (trial_dur / trial_nums.count(trial_num)))
                hz_arr.append(hz_last_trial)
                print("Hz last trial: {}".format(hz_last_trial))

        # end exp loop
        if check_for_event("End", exp_start_time):
            print("Exp loop ended")
            exp_started = False

    # exp loop finished
    df = pd.DataFrame()
    df["X"] = x
    df["Y"] = y
    df["Z"] = z
    
    # only add w column to df if orientation recorded
    if not record_pos:
        df["W"] = w
    
    df["Time"] = times
    df["TrialNum"] = trial_nums
    
    # only give info and save file if something was recorded
    if len(hz_arr) > 0:
        # give info about recordings
        hz_mean = np.round(np.mean(hz_arr), 2)
        hz_sd = np.round(np.std(hz_arr), 2)
        hz_max = max(hz_arr)
        hz_min = min(hz_arr)
        print("Number of trials: {}, Mean Hz: {}, SD Hz: {}, Max Hz: {}, Min Hz: {}".format(trial_num, hz_mean, 
                                                                                            hz_sd, hz_max, hz_min))
    
        # save data
        print("Saving file")
        df.to_csv(filename)
    else:
        print("No data was recorded")

def main():
    # adjustment parameters
    record_pos = True # record position (True) or orientation (False)
    
    # subjectID for log file name
    subject_id = input('Enter SubjectID: ')
    
    # prep file name
    now = datetime.now()
    date = "{}.{}.{}_{}_{}".format(now.day, now.month, now.year, now.hour, now.minute)
    f_name = "log_{}_{}".format(subject_id, date)
    
    # prep file directory
    save_dir = Path.absolute(Path.cwd().parent.joinpath("Z logs"))
    if not save_dir.exists():
        save_dir.mkdir(parents=True, exist_ok=True)
    save_dir = str(save_dir)
    
    # create complete path and give info
    filename = "{}\\{}.csv".format(save_dir, f_name)
    print("Saving at: {}".format(filename))
    
    # data collection loop
    exp_loop(filename, record_pos)
    
    # stop the app
    _ = input('Press enter to exit')
    sys.exit()
    
main()