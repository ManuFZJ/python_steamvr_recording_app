import subprocess
from pathlib import Path

txt_path = str(Path.absolute(Path.cwd())) + "\\requirements.txt"
subprocess.call(['pip', 'install', "-r", txt_path])
print("All necessary packages installed")
_ = input('Press any button to exit')