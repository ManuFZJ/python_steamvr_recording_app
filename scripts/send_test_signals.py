#!/usr/bin/env python
# coding: utf-8

import sys
import win32evtlogutil
import win32evtlog
import time

def accurate_delay(delay):
    _ = time.perf_counter() + delay/1000
    while time.perf_counter() < _:
        pass

# get input for the test signals
num_signals = input('Enter number of trials to send signals for: ')
try:
    num_signals = int(num_signals)
except:
    print("Input could not be converted to an integer, using 10 trials")
    num_signals = 10
    
trial_dur = input('Enter trial duration in ms: ')
try:
    trial_dur = int(trial_dur)
except:
    print("Input could not be converted to an integer, using 1000ms")
    trial_dur = 1000

# send signals
id = 7040  # can be any number as far as I can tell
num_signals = num_signals + 1 # have to add 1 more due to range()
for num in range(1,num_signals):
    # TrialStart
    event_name = "StimulusOnset" # name to search for
    win32evtlogutil.ReportEvent(event_name, id, eventType=win32evtlog.EVENTLOG_WARNING_TYPE)

    # Delay
    t_start = time.perf_counter()
    accurate_delay(trial_dur)
    t_end = time.perf_counter()

    # TrialEnd
    event_name = "ResponseEvent"
    win32evtlogutil.ReportEvent(event_name, id, eventType=win32evtlog.EVENTLOG_WARNING_TYPE)
    
    # give info about the simulated trial
    print('Start time: {:.5f}, End time: {:.5f}, Delay is {:.5f} ms'.format(t_start, t_end, 1000*(t_end - t_start)))

# End
accurate_delay(1000) # use delay to ensure it is found after trial was properly finished, 1 s
event_name = "End"
win32evtlogutil.ReportEvent(event_name, id, eventType=win32evtlog.EVENTLOG_WARNING_TYPE)